module Main where
import Parser 
import Data.Maybe
import Data.Monoid
import Data.List

main :: IO ()
main = undefined


  
-- To carry messages about the evaluation
data Eval a = Done (StdExpr a) (Eval a)
            | ErrorMsg [String]
            | Message [String]
            | NoError            
  deriving (Show, Eq)

-- Pretty print a StdExpr
data PrettyExpr a = Pretty (StdExpr a)

-- Standard Expression type
data StdExpr a = Null
                 -- Evaluation return value
               | Return (StdExpr a) (Eval a)
               -- Not defined
               | Func (StdExpr a) (StdExpr a)
               | Error
                 -- Boolean
               | BoolT
               | BoolF
                 -- Variable and Argument
               | Var String (StdExpr a)
                 -- Function
               | Keyword String
               | Atom String
                 -- Lists
               | Cons (StdExpr a) (StdExpr a)
               | Sym  (StdExpr a) (StdExpr a)
               | ListStr [(StdExpr String)]
               | ListInt [(StdExpr Integer)]
               | ListDub [(StdExpr Double)]
               | List [(StdExpr a)]
                 -- Numbers
               | NumInt Integer 
               | NumDub Double
                 -- Errors
                 -- Will skip being evaluated
               | Symbol String
               | Quote (StdExpr a)
  deriving (Eq, Show)

data LambdaExpr a =
  LambdaExpr {args  :: [(String, (StdExpr a))],
              scope :: [(String, (StdExpr a))],
              body  :: (StdExpr a)}

instance Show (PrettyExpr a) where
  show a = case a of
    (Pretty a) -> showStdExpr "" a


-- Return everything before any character in breaks, from text
beforeSoP :: String -> String -> String
beforeSoP "" breaks = ""
beforeSoP text@(t:ts) breaks
  | elem t breaks = ""
  | otherwise     = t : beforeSoP ts breaks





-- Split a string into a list of strings separated by a character.
split :: Char -> [Char] -> [[Char]]
split c text = split_iter c text []
  where
    -- Split a string given a character, string and empty list.
    split_iter :: Char -> [Char] -> [Char] -> [[Char]]
    split_iter c "" carry = carry : []
    split_iter c (x:xs) carry
      | c == x = carry : split_iter c xs ""
      | otherwise = split_iter c xs (carry ++ [x])
      
level :: String -> String -> String -> [(Int, String)]
level start end text = level_iter start end 0 text []
  where -- LevelHelper with accumulator
    level_iter :: String -> String -> Int -> String -> String -> [(Int, String)]
    level_iter start end depth "" carry = [(depth, carry)]
    level_iter start end depth (t:ts) carry
      | elem t start = [(depth, carry)] ++ level_iter start end (depth + 1) ts ""
      | elem t end = [(depth, carry)] ++ level_iter start end (depth - 1) ts ""
      | otherwise = level_iter start end depth ts (carry ++ [t])




{- untilMatched paranCount text
   Get all characters util the amount of paranCount == 0 from text
including last closeParan
ex: "define (square x) (* x x))" -> "define (square x) (* x x))"
    "square x) (* x x))"         -> square x)
-}




-- Keywords (Built in functions)
keywords = getKeywords keywordTable 

-- Lisp list tools
--car (Cons a b) = a
--cdr (Cons a b) = b

-- Get function and arguments from a inner expression as a tuple
-- (Fun, List of arguments)
getFunArgs :: StdExpr a -> (StdExpr a, StdExpr a)
getFunArgs (List expr) =
  if (tail expr) == [] then (head expr, Null)
  else (head expr, List $ tail expr)
getFunATgs _           =
  let errorMessage = Return Error $ Message ["getFunArgs", "Expression is not a List"]
  in (errorMessage, errorMessage)

-- Get a Keywords string value
keywordToString (Keyword str) = str
-- If list has length 1 return head list else list
argFormat (List l) = if (length l == 1) then head l else List l
isReturn (Return a _) = True
isReturn _            = False

getExpr (Return a _)  = a
getExpr a             = a


-- Is the expr a List?
isList :: StdExpr a -> Bool
isList (List a) = True
isList _        = False

-- Does the List contain other lists?
isInnerExprL :: StdExpr a -> Bool
isInnerExprL (List expr) =
  if (head expr) == (Keyword "lambda") -- Hack to allow lambda expressions as an inner expression
  then True
  else foldr (\a b -> (isItem a) && b) True expr
isInnerExprL _ = False

-- Is first element of A List (StdExpr) a Keyword?
isFirstKeywordL :: StdExpr a -> Bool
isFirstKeywordL (List expr) =
  let first = head expr
  in case first of
       (Keyword first) -> True
       otherwise       -> False

isCons :: StdExpr a -> Bool
isCons (Cons car cdr) = True
isCons _              = False




evalExpr :: StdExpr a -> StdExpr a
evalExpr expr 
  | isInnerExprL expr = getExpr $ evalInnerExpr expr
  | isReturn expr     = getExpr expr
  | isList expr       = evalExpr $ (List $ map evalExpr (unList expr))
  | isItem expr       = expr
  | isCons expr       = expr
  | otherwise         = Null


-- Evaluate an inner expression, etc
evalInnerExpr :: StdExpr a -> StdExpr a
evalInnerExpr expr =
  -- Is expr a inner expression?
  if (isList expr && isInnerExprL expr && isFirstKeywordL expr)
  then let funAndArgs = getFunArgs expr
           fun        = keywordFindKW (fst funAndArgs)
           args       = (snd funAndArgs)
       in case fun of
            Nothing -> Return Error (Message ["evalInnerExpr",
                                              "Cannot find a defined function for keyword",
                                              keywordToString (fst funAndArgs)])
            (Just f)-> f $ argFormat args

  else Return Error (Message ["evalInnerExpr", "Not a correct inner expression"])


-- Determine if a String is a Number (Integer)
-- https://stackoverflow.com/questions/30029029/haskell-check-if-string-is-valid-number
isNumber :: String -> Bool
isNumber str =
    case (reads str) :: [(Integer, String)] of
      [(_, "")] -> True
      _         -> False


-- Categorize and convert a String to most fitting item in StdExpr a 
categorize :: String -> StdExpr a
categorize value
  | isNumber value      = NumInt $ read value
  | (head value) == '-' && isNumber (tail value) = NumInt (-1 * (read value))
  | elem value keywords = Keyword value
  | (head value) == ':' = Symbol value
  | (head value) == '`' = Symbol value
  | value == "False"    = BoolF
  | value == "True"     = BoolT
  | value == "Null"     = Null
  | otherwise           = Atom value



cons a b = Cons a b -- Construct a Cons of a and b
cend     = Null     -- End of a Cons list

-- Remove unnessisary layer around expr when not wanted
remLayer (Cons a Null) = a 
remLayer a             = a

-- Create a List (StdExpr) from a haskell list ([])
toList a = List a
unList (List a) = a

numIntD (NumInt a) = a
breakoutList (List a) = a

apply :: (StdExpr a -> StdExpr a) -> StdExpr a -> Eval a
apply f a = undefined
-- Keyword function lookup table


-- Is the number e in the of NumInt l?
numInList e l = foldr (\a b -> ((numIntD a) == e) || b) False l


-- If keyword is found, 
-- then return (keywordFun :: (StdExpr a b -> StdExpr a b))
-- otherwise return (ErrorFunc :: (StdExpr a b -> ErrorExpr a b))
keywordFind :: String ->  Maybe (StdExpr a -> StdExpr a)
keywordFind key = lookup key keywordTable

keywordFindKW :: StdExpr a -> Maybe (StdExpr a -> StdExpr a)
keywordFindKW (Keyword str) = keywordFind str

-- Extract StdExpr from Eval a (StdExpr b)
escKwFind :: Maybe (StdExpr b -> StdExpr a) -> (StdExpr b -> StdExpr a)
escKwFind a = case a of
  (Just expr) -> expr
  Nothing     -> (\a -> Return Error $ (ErrorMsg ["escKwFind", "Nothing found"]))


showEval s (Message e) = s ++ "\nMESSAGE: "++ (foldr (\a b -> a ++ ", " ++ b) "" e)
showEval s _ = ""
-- More pretty print of a StdExpr, use "Pretty expr" where
-- expr::(StdExpr a)
showStdExpr :: String -> StdExpr a -> String
showStdExpr s Null        = s ++ "Null"
showStdExpr s Error       = s ++ "Error"
showStdExpr s (Cons a b)  = s ++ "(" ++ (showStdExpr "" a) ++ " . " ++ (showStdExpr "" b) ++ ")"
showStdExpr s (List a)    =
  let rest  = drop 1 a
      first = take 1 a
  in s ++ "(" ++ (showStdExpr "" (head first)) ++ concat (map (showStdExpr " ") rest)  ++  ")"
showStdExpr s (Keyword a) = s ++ a
showStdExpr s (Atom a)    = s ++ a
showStdExpr s (NumInt a)  = s ++ show a
showStdExpr s (Symbol a)  = s ++ show a
showStdExpr s (Return a b)= s ++ "Return " ++ showStdExpr "" a ++ showEval " " b 
showStdExpr s (Var a b)   = s ++ "(:= " ++ show a ++ show b ++ ")"
showStdExpr s (Func a b)   =
  let args  = consToList a
      body  = consToList b
  in s ++ "(λ " ++ "(" ++ (showStdExpr "" $ head args) ++
  concat (map (showStdExpr " ")  $ tail args) ++ ") " ++
  "(" ++ (showStdExpr "" $ head body) ++
  concat (map (showStdExpr " ")  $ tail body) ++ "))"
  
showStdExpr s a           = s ++ "!!UNDEFINED PRINT!!"


{- showResult expr
   Print the StdExpr with the message attatched
   EXAMPLES:
     showResult $ (escKwFind (keywordFind "NotAKeyword")) $ List [NumInt 10, NumInt 20]
   ->"Error {Path -> escKwFind-> Nothing as argument}"
-}
showResult :: StdExpr a -> String
showResult (Return a (ErrorMsg b)) =
  (showStdExpr "" a) ++ " {Error " ++ concat (map ("=> "++) b) ++ "}"
showResult (Return a (Message c))  =
  (showStdExpr "" a) ++ " {Apply " ++ concat (map ("=> "++) c) ++ "}"




isItem (Atom a) = True
isItem (Keyword a) = True
isItem (NumInt a) = True
isItem (Quote a) = True
isItem (Symbol a) = True
isItem BoolF = True
isItem BoolT = True
isItem _ = False

-- Hack to allow stringToExpr to return Lists of Lists (StdExpr)
-- instead of Cons 
listAppend (List a) Null = List $ [List a]
listAppend a Null        = a

listAppend (List a) (List b) = List $ [(List a)] ++ b
listAppend (List a) b = List $ [List a] ++ [b]
listAppend a (List b) = List $ [a] ++ b
listAppend a b        = List $ [a] ++ [b]
  

-- Tokenize String -> Cons expression
tokenizeStr str = stringToExpr cons cend remLayer categorize str
-- Tokenize String -> List expression
tokenizeStrL str = stringToExpr listAppend Null (\a -> a) categorize str

-- Turn a Cons list into a Haskell list
consToList = consToList'
  where
    consToList' Null          = []
    consToList' (Cons a b)    = consToList a ++ consToList b
    consToList' a             = [a]

getKeywords :: [(String, StdExpr a -> (StdExpr a))] -> [String]
getKeywords kwTable = map fst kwTable

-- ###################### PREDEFINED FUNCTIONS (Keywords) ###########################
keywordTable :: [(String, StdExpr a -> (StdExpr a))]
keywordTable =  [("+", addIntL),
                 ("-", subIntL),
                 ("*", multIntL),
                 ("/", divIntL),
                 ("neg", negInt),
                 ("abs", absInt),
                 ("rem", remIntL),
                 ("lambda", defLambda),
                 ("cons", createConsL)]
  where
    -- Cons
    createConsL :: (StdExpr a) -> (StdExpr a)
    createConsL (List expr) =
      if (length expr /= 2)
      then Return Error (ErrorMsg ["createCons", "the Cons function can only take 2 arguments"])
      else let first  = (expr !! 0)
               second = (expr !! 1)
           in  Return (Cons first second) $ Message ["createCons", "Successfully created Cons", (showStdExpr " car:" first) ++ (showStdExpr " cdr: " second)]

    
    -- Create Lambda
    defLambda :: (StdExpr a) -> (StdExpr a)
    defLambda (List expr) =
      let args = expr !! 0
          body = expr !! 1
      in Return (Func args body) (Message ["lambda", showStdExpr "" args, showStdExpr "" body])
    defLambda a = Return Error (ErrorMsg ["lambda", showStdExpr "" $ a])

    -- Negate a NumInt value 
    negInt :: (StdExpr a) -> (StdExpr a)
    negInt (NumInt a) = Return (NumInt (-a)) (Message ["- ", showStdExpr "" $ NumInt a])

    -- Absolute of a NumInt value 
    absInt :: (StdExpr a) -> (StdExpr a)
    absInt (NumInt a) = Return (NumInt (abs a)) (Message ["abs ", showStdExpr "" $ NumInt a])


    -- #################### MATH ON CONS ######################

    -- Add
    addConsNumInt :: (StdExpr a) -> (StdExpr a)
    addConsNumInt expr = Return (NumInt $ allConsNumInt' expr (+) 0) $ Message ["+ ", showStdExpr "" expr]

    -- Multiply 
    multConsNumInt :: (StdExpr a) -> (StdExpr a)
    multConsNumInt expr = Return (NumInt $ allConsNumInt' expr (*) 1) $ Message ["* ", showStdExpr "" expr]

    -- General function to applying a function to a whole cons list
    allConsNumInt' :: (StdExpr a) -> (Integer -> Integer -> Integer) -> Integer -> Integer
    allConsNumInt' (Cons (NumInt a) b) f d = f a (allConsNumInt' b f d)
    allConsNumInt' b f d                   = d
  
    -- True if two numbers are equal (Cons)
    eqNumInt :: (StdExpr a) -> (StdExpr a)
    eqNumInt (Cons (NumInt a) (Cons (NumInt b) Null)) =
      Return (stdBool (a == b)) (Message ["== ", showStdExpr "" $ NumInt a])

    stdBool :: Bool -> StdExpr a
    stdBool a = if a then BoolT else BoolF
   
    -- #################### MATH ON LISTS ######################

    -- Show arguments as a String
    showNumIntArg a = (showStdExpr "List of NumInts: " $ head a) ++ concat (map (showStdExpr " ") $ tail a) 

    -- Apply function using foldr on list of type List [NumInt]
    numIntLl :: (Integer -> Integer -> Integer) ->
               Integer ->   -- Initvalue needed for foldl
               StdExpr a -> -- Arguments
               ([StdExpr a] -> Eval a) -> -- Message on successful function evaluation
               ([StdExpr a] -> Eval a) -> -- Message failure on function evaluation
               (StdExpr a)
    numIntLl f initVal (List numInts) successMsg failureMsg = -- Success
      Return (NumInt (foldl f initVal $ map numIntD numInts)) $ successMsg numInts

    numIntLl f  _ numIntList _ failureMsg = -- Error
      Return numIntList (failureMsg $ unList numIntList)

    -- Apply function using foldl on list of type List [NumInt]
    numIntLr :: (Integer -> Integer -> Integer) ->
               Integer ->   -- Initvalue needed for foldl
               StdExpr a -> -- Arguments
               ([StdExpr a] -> Eval a) -> -- Message on successful function evaluation
               ([StdExpr a] -> Eval a) -> -- Message failure on function evaluation
               (StdExpr a)
    numIntLr f initVal (List numInts) successMsg failureMsg = -- Success
      Return (NumInt (foldr f initVal $ map numIntD numInts)) $ successMsg numInts

    numIntLr f  _ numIntList _ failureMsg = -- Error
      Return Error (failureMsg $ unList numIntList)

    -- NumInt Algebraic functions
    addIntL :: (StdExpr a) -> (StdExpr a)
    addIntL expr = numIntLl (+) 0 expr
                   (\a -> Message ["addIntL", "Successfully added", showNumIntArg a])
                   (\a -> Message ["addIntL", "Wrong type", "Function error"])

    -- Subtract all values of a list
    subIntL :: (StdExpr a) -> (StdExpr a)
    subIntL expr = numIntLr (-) 0 expr
                   (\a -> Message ["subIntL", "Successfully subtracted", showNumIntArg a])
                   (\a -> Message ["subIntL", "Wrong type", "Function error"])

    -- Multiply all values of a list
    multIntL :: (StdExpr a) -> (StdExpr a)
    multIntL expr = numIntLl (*) 1 expr
                    (\a -> Message  ["multIntL", "Successfully multiplied", showNumIntArg a])
                    (\a -> Message ["multIntL", "Wrong type", "Function error"])

    -- Divide a list ex: div [1, 2, 3, 4, 5] -> (((((1 / 2) / 3) / 4) / 5) / 1)
    divIntL :: (StdExpr a) -> (StdExpr a)
    divIntL expr = if (numInList 0 (tail (unList expr))) -- Exception for division by zero
                   then Return Error $ Message ["divIntL", "Division by zero", "Function error"]
                   else numIntLr (div) 1 expr
                        (\a -> Message  ["divIntL", "Successfully divided", showNumIntArg a])
                        (\a -> Message ["divIntL", "Wrong type", "Function error"])


    -- Remainder of the first two values of a list, rem [a, b, c, d..] -> rem a b
    remIntL :: (StdExpr a) -> (StdExpr a)
    remIntL (List expr) = Return (NumInt (rem (numIntD $ expr !! 0) (numIntD $ expr !! 1)))
                                 (Message ["remIntL", "successfully calculated remainder of the first two values from", showNumIntArg expr])
    remIntL a = Return Error $ Message ["remIntL", "Uncorrectly formated args"]

    

        
